# Verilated -*- Makefile -*-
# DESCRIPTION: Verilator output: Make include file with class lists
#
# This file lists generated Verilated files, for including in higher level makefiles.
# See Vsimtop.mk for the caller.

### Switches...
# C11 constructs required?  0/1 (always on now)
VM_C11 = 1
# Coverage output mode?  0/1 (from --coverage)
VM_COVERAGE = 0
# Parallel builds?  0/1 (from --output-split)
VM_PARALLEL_BUILDS = 1
# Threaded output mode?  0/1/N threads (from --threads)
VM_THREADS = 0
# Tracing output mode?  0/1 (from --trace/--trace-fst)
VM_TRACE = 0
# Tracing output mode in VCD format?  0/1 (from --trace)
VM_TRACE_VCD = 0
# Tracing output mode in FST format?  0/1 (from --trace-fst)
VM_TRACE_FST = 0
# Tracing threaded output mode?  0/1/N threads (from --threads/--trace-thread)
VM_TRACE_THREADS = 0
# Separate FST writer thread? 0/1 (from --trace-fst with --trace-thread > 0)
VM_TRACE_FST_WRITER_THREAD = 0

### Object file lists...
# Generated module classes, fast-path, compile with highest optimization
VM_CLASSES_FAST += \
	Vsimtop \
	Vsimtop___024root__DepSet_h3a8afb71__0 \
	Vsimtop___024root__DepSet_h305f3046__0 \
	Vsimtop___024root__DepSet_h305f3046__1 \
	Vsimtop___024root__DepSet_h305f3046__2 \
	Vsimtop___024root__DepSet_h305f3046__3 \
	Vsimtop___024root__DepSet_h305f3046__4 \
	Vsimtop___024root__DepSet_h305f3046__5 \

# Generated module classes, non-fast-path, compile with low/medium optimization
VM_CLASSES_SLOW += \
	Vsimtop__ConstPool_0 \
	Vsimtop___024root__Slow \
	Vsimtop___024root__DepSet_h305f3046__0__Slow \
	Vsimtop___024root__DepSet_h305f3046__1__Slow \
	Vsimtop___024root__DepSet_h305f3046__2__Slow \
	Vsimtop___024root__DepSet_h305f3046__3__Slow \
	Vsimtop___024root__DepSet_h305f3046__4__Slow \
	Vsimtop___024root__DepSet_h305f3046__5__Slow \
	Vsimtop___024unit__Slow \
	Vsimtop___024unit__DepSet_h7e859b80__0__Slow \

# Generated support classes, fast-path, compile with highest optimization
VM_SUPPORT_FAST += \

# Generated support classes, non-fast-path, compile with low/medium optimization
VM_SUPPORT_SLOW += \
	Vsimtop__Syms \

# Global classes, need linked once per executable, fast-path, compile with highest optimization
VM_GLOBAL_FAST += \
	verilated \

# Global classes, need linked once per executable, non-fast-path, compile with low/medium optimization
VM_GLOBAL_SLOW += \


# Verilated -*- Makefile -*-
